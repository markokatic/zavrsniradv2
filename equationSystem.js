class EquationSystem {

    linearEquation = null;
    quadraticEquation = null;

    constructor(linear,quadratic){
        this.linearEquation = linear;
        this.quadraticEquation = quadratic;
    }

    findIntersections(){
        let quadraticValues = this.quadraticEquation.getEquationValues();
        let linearValues = this.linearEquation.getEquationValues();

        let f = linearValues[0][0];
        let h = linearValues[1][0];

        let x12;
        let y12;

        if(f!=0 && h!=0){
            x12 = this.getXCoordinateIntersection(quadraticValues,linearValues);
            y12 = this.getYCoordinateIntersections(quadraticValues,linearValues);
            return [[x12[0],y12[0]],[x12[1],y12[1]]];
        }
        if(f!=0){
            y12 = this.getYCoordinateIntersections(quadraticValues,linearValues);
            return [[this.linearEquation.getX(y12[0]),y12[0]],[this.linearEquation.getX(y12[1]),y12[1]]]
        }
        if(h!=0){
            x12 = this.getXCoordinateIntersection(quadraticValues,linearValues);
            return [[x12[0],this.linearEquation.getY(x12[0])],[x12[1],this.linearEquation.getY(x12[1])]]
        }

        return null;
    }
    
    getXCoordinateIntersection(quadraticValues,linearValues){
        let a = quadraticValues[2][0];
        let b = quadraticValues[3][0];
        let c = quadraticValues[0][0];
        let d = quadraticValues[1][0];
        let e = quadraticValues[4][0];
        
        let f = linearValues[0][0];
        let h = linearValues[1][0];
        let i = linearValues[2][0];

        let A = (f*f*d)/(h*h)+c;
        let B = (2*d*f*i)/(h*h)-(b*f)/h+a;
        let C = e + (i*i*d)/(h*h) - (f*i)/h;
        if(A!=0){
            return [(-B+ Math.sqrt(B*B-4*A*C))/(2*A),(-B - Math.sqrt(B*B-4*A*C))/(2*A)]
        }
        if(B!=0){
            return [(-C/B),(-C/B)];
        }
        return [NaN,NaN];
        
    }

    getYCoordinateIntersections(quadraticValues,linearValues){
        let a = quadraticValues[2][0];
        let b = quadraticValues[3][0];
        let c = quadraticValues[0][0];
        let d = quadraticValues[1][0];
        let e = quadraticValues[4][0];
        
        let f = linearValues[0][0];
        let h = linearValues[1][0];
        let i = linearValues[2][0];

        let A = (c*h*h)/(f*f)+d;
        let B = b-(h*a)/f + (2*c*h*i)/(f*f);
        let C = -(i*a)/f + (i*i*c)/(f*f) + e;
        if(A!=0){
        return [(-B+ Math.sqrt(B*B-4*A*C))/(2*A),(-B - Math.sqrt(B*B-4*A*C))/(2*A)]
        }
        if(B!=0){
            return [(-C/B),(-C/B)];
        }
        return [NaN,NaN];
        
    }

}