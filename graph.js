class Graph{

    graph;
    canvas;
    xAxisSign = 'x';
    yAxisSign = 'y';

    step = 1;
    numberOfSteps = 22;
    pixelsPerStep = 0;
    xStart = -10;
    yStart = -10;

    lineColor = "#FF0000";
    curveColor = "#0000FF";


    constructor(canvas){
        this.graph = canvas.getContext('2d');
        this.canvas = canvas;
        this.drawCoordinateSystem();
        this.pixelsPerStep = ((this.canvas.width)/this.numberOfSteps)/this.step;
    }


    drawCoordinateSystem(){
        
        var canvasHeight = this.canvas.height
        var canvasWidth = this.canvas.width
        var context = this.graph;
        context.font = "14px Arial"
        context.strokeStyle="#000000"
        context.beginPath()
        context.moveTo(0,(canvasHeight/2))
        context.lineTo(10,(canvasHeight/2)+10)
        context.moveTo(0,(canvasHeight/2))
        context.lineTo(10,(canvasHeight/2)-10)
        context.moveTo(0,(canvasHeight/2))
        context.lineTo(canvasWidth,(canvasHeight/2))
        context.lineTo(canvasWidth-10,(canvasHeight/2)+10)
        context.moveTo(canvasWidth,(canvasHeight/2))
        context.lineTo(canvasWidth-10,(canvasHeight/2)-10)
        context.stroke()
        
        context.beginPath()
        context.moveTo((canvasWidth/2),0)
        context.lineTo((canvasWidth/2)-10,10)
        context.moveTo((canvasWidth/2),0)
        context.lineTo((canvasWidth/2)+10,10)
        context.moveTo((canvasWidth/2),0)
        context.lineTo((canvasWidth/2),canvasHeight)
        context.lineTo((canvasWidth/2)-10,canvasHeight-10)
        context.moveTo((canvasWidth/2),canvasHeight)
        context.lineTo((canvasWidth/2)+10,canvasHeight-10)
        context.stroke()
        let j = 0;
        var i = 0;
        context.beginPath()
        for(i = canvasWidth/this.numberOfSteps;i<=canvasWidth-canvasWidth/this.numberOfSteps;i = i + canvasWidth/this.numberOfSteps)
        {
            context.moveTo(i,(canvasHeight/2)-5)
            context.lineTo(i,(canvasHeight/2)+5)
            if(j!=this.numberOfSteps/2-1){
                context.fillText((this.xStart+j*this.step).toString(),i-6,canvasWidth/2+18)
            }
            j++;
            
        }
        context.stroke()
        context.strokeStyle = "#000000"
        context.beginPath()
        j = 0;
        for(i = canvasHeight-canvasHeight/this.numberOfSteps;i >=canvasHeight/this.numberOfSteps;i = i - canvasHeight/this.numberOfSteps)
        {
            context.moveTo((canvasWidth/2)-5,i)
            context.lineTo((canvasWidth/2)+5,i)
          

        if(j!=this.numberOfSteps/2-1){
            context.fillText((this.yStart+j*this.step).toString(),canvasHeight/2+15,i+5 )
        }
        j++;
        }
        context.strokeStyle = "#000000"
        context.stroke()

        
        context.fillText("+"+this.xAxisSign,canvasWidth-18,canvasHeight/2+30)
        context.fillText("-"+this.xAxisSign,0,canvasHeight/2+30)
        context.fillText("+"+this.yAxisSign,canvasWidth/2-30,12)
        context.fillText("-"+this.yAxisSign,canvasWidth/2-30,canvasHeight-5)




    }

    getNumberOfSteps(){
        return this.numberOfSteps;
    }

    getStep(){
        return this.step;
    }

    getStartX(){
        return this.xStart;
    }

    getStartY(){
        return this.yStart;
    }

    setStep(step){
        this.step = step;
        this.pixelsPerStep = this.canvas.width/(this.step*this.numberOfSteps);
    }

    setXStart(x){
        this.xStart = x;
    }
    setYStart(y){
        this.yStart = y;
    }

    setAxisSigns(xAxis,yAxis){
        this.xAxisSign = xAxis;
        this.yAxisSign = yAxis;
    }

    setLineColor(color){
        this.lineColor = color;
    }

    setCurveColor(color){
        this.curveColor = color;
    }

    getPixelX(x){
        return (x-this.xStart)*this.pixelsPerStep+this.canvas.width/this.numberOfSteps;
    }
    getPixelY(y){
        return this.canvas.height-(y-this.yStart)*this.pixelsPerStep-this.canvas.height/this.numberOfSteps;
    }

    drawLine(x1,y1,x2,y2){
        this.graph.beginPath();
        this.graph.strokeStyle = this.lineColor;
        this.graph.moveTo(this.getPixelX(x1),this.getPixelY(y1));
        this.graph.lineTo(this.getPixelX(x2),this.getPixelY(y2));
        this.graph.stroke();
    }

    drawCurve(coordinatesArray){
        if(coordinatesArray.length == 0){return;}
        this.graph.beginPath();
        this.graph.strokeStyle = this.curveColor;
        this.graph.moveTo(this.getPixelX(coordinatesArray[0][0]),this.getPixelY(coordinatesArray[0][1]));
        let prevX=null;
        let prevY=null;
        for(let i = 1; i<coordinatesArray.length;i++){
            let xPix = this.getPixelX(coordinatesArray[i][0]);
            let yPix = this.getPixelY(coordinatesArray[i][1]);
            if(prevX!=null && prevY!=null){
                if(Math.abs(xPix-prevX)>1 || Math.abs(yPix-prevY)>10){
                    this.graph.moveTo(xPix,yPix);
                }
                else{
                    this.graph.lineTo(xPix,yPix);
                }
            }else{
                this.graph.lineTo(xPix,yPix);
            }
            prevX = xPix;
            prevY = yPix;
        }
        this.graph.stroke();
    }

    clear(){
        this.graph.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
}
