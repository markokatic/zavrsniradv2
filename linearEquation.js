class LinearEquation{
    equationString = "";
    equationValues = [[0,"x"],[0,"y"],[0,""]];

    constructor(equationString){
        this.equationString = equationString;
        this.setVariables();
    }

    getY(x){
        if(this.equationValues[1][0]!=0)
        {
            return (-x*this.equationValues[0][0]-this.equationValues[2][0])/this.equationValues[1][0];
        }
        return NaN;
    }
    getX(y){
        if(this.equationValues[0][0]!=0)
        {
            return (-y*this.equationValues[1][0]-this.equationValues[2][0])/this.equationValues[0][0];
        }
        return NaN;
    }

    getEquationValues(){
        return this.equationValues;
    }

    setVariables() {
      this.equationString = this.equationString.replaceAll(' ',''); 
      let values = this.equationString.split(/[=+\-]/).filter(element => element.length > 0);
      let equationIndex = this.equationString.search('=');
      let equationString = this.equationString; 

      values.forEach((element)=>{
          let index = equationString.search(element);
          equationString = equationString.replace(element,'');
          equationIndex = equationString.search('=');
          for (let i = 0; i < this.equationValues.length; i++) {
              const val = this.equationValues[i]; 
              if (element.includes(val[1])) {
                element = element.replace(val[1], '');
                let elementValue = parseFloat(element);
                
                if(isNaN(elementValue)){
                  elementValue=1;
                }

                if (index - 1 >= 0) {
                  if (equationString[index - 1] === '-') {
                    elementValue *= -1;
                  }
                  
                  if (index > equationIndex) {
                    elementValue *= -1;
                  }
                }
                val[0] = elementValue;
                break;
              }
            }
      });
  }
}