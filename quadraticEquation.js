class QuadraticEquation{

    equationString = "";
    equationValues = [[0,"x2"],[0,"y2"],[0,"x"],[0,"y"],[0,""]];

    constructor(equationString){
        this.equationString = equationString;
        this.setVariables();
    }

    getX(y){
        let c = this.equationValues[0][0];
        let d = this.equationValues[1][0];
        let a = this.equationValues[2][0];
        let b = this.equationValues[3][0];
        let e = this.equationValues[4][0];

        if(2*c==0){return[NaN,NaN];}

        return [
            (-a+Math.sqrt(a*a-4*c*(b*y+d*y*y+e)))/(2*c),
            (-a-Math.sqrt(a*a-4*c*(b*y+d*y*y+e)))/(2*c)
        ]
    }

    getY(x){
        let c = this.equationValues[0][0];
        let d = this.equationValues[1][0];
        let a = this.equationValues[2][0];
        let b = this.equationValues[3][0];
        let e = this.equationValues[4][0];

        if(2*d==0){return[NaN,NaN];}

        return [
            (-b+Math.sqrt(b*b-4*d*(a*x+c*x*x+e)))/(2*d),
            (-b-Math.sqrt(b*b-4*d*(a*x+c*x*x+e)))/(2*d)
        ]
    }

    isHiperbola() {
      const c = this.equationValues[0][0];
      const d = this.equationValues[1][0];
      if ((c < 0 && d > 0) || (c > 0 && d < 0)) {
          return true;
      } 
      return false;
    }

    hiperbolaOrientation(){
      if(this.equationValues[0][0]>0 && this.equationValues>[4][0]>0){
        return true;
      }
      return false;
    }

    getEquationValues(){
      return this.equationValues;
    }

    setVariables() {
        this.equationString = this.equationString.replaceAll(' ',''); 
        let values = this.equationString.split(/[=+\-]/).filter(element => element.length > 0);
        let equationIndex = this.equationString.search('=');
        let equationString = this.equationString; 

        values.forEach((element)=>{
            if(element != "0"){
              let index = equationString.search(element);
              equationString = equationString.replace(element,'');
              equationIndex = equationString.search('=');
              for (let i = 0; i < this.equationValues.length; i++) {
                  const val = this.equationValues[i]; 
                  if (element.includes(val[1])) {
                    element = element.replace(val[1], '');
                    let elementValue = parseFloat(element);
                    
                    if(isNaN(elementValue)){
                      elementValue=1;
                    }
  
                    if (index - 1 >= 0) {
                      if (equationString[index - 1] === '-') {
                        elementValue *= -1;
                      }
                      
                      if (index > equationIndex) {
                        elementValue *= -1;
                      }
                    }
                    val[0] = elementValue;
                    break;
                  }
                }
            }
        });
    }

}