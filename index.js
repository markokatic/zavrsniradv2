let quadratic = null;
let linear = null;
let graph = new Graph((document.getElementById("canvasId")));
let system = null;
let resultContainer = document.getElementById("result");
let result = null;
function calculate() {

    let linearNonParsed = document.getElementById("linearEquationInput").value;
    let quadraticNonParsed = document.getElementById("quadraticEquationInput").value;

    let linearSigns = findUniqueLetters(linearNonParsed);
    let quadraticSigns = findUniqueLetters(quadraticNonParsed);

    let signs = sortAndMerge(linearSigns,quadraticSigns);
    
        if(signs.length<=2 && signs.length>0 && linearNonParsed!="" && quadraticNonParsed!=""){
            graph.setAxisSigns(signs[0],signs[1]);
    
            let linearParsed = linearNonParsed.replaceAll(signs[0],'x');
            linearParsed = linearParsed.replaceAll(signs[1],'y');
            let quadraticParsed = quadraticNonParsed.replaceAll(signs[0],'x');
            quadraticParsed = quadraticParsed.replaceAll(signs[1],'y');
        
        
            quadratic = new QuadraticEquation(quadraticParsed);
            linear = new LinearEquation(linearParsed);
            system = new EquationSystem(linear,quadratic);
            result = system.findIntersections();
            if(isQuadraticSolvable()){
                writeResult();
                paint(true);}
            else{
                resultContainer.innerHTML = "Kvadratna jednadžba nije ispravno unešena";
                paint(true);
            }
        }
        else {
            resultContainer.innerHTML = "Unos jednadžbi nije ispravan";
        }   
}

function moveLeft(){
    graph.setXStart(graph.getStartX()+graph.getStep());
    if(quadratic!=null && linear!=null){
        paint(false);
    }
    else{
        graph.clear()
        graph.drawCoordinateSystem();
    }
   
}

function moveRight(){
    graph.setXStart(graph.getStartX()-graph.getStep());
    if(quadratic!=null && linear!=null){
        paint(false);
    }
    else{
        graph.clear()
        graph.drawCoordinateSystem();
    }
}

function moveUp(){
   graph.setYStart(graph.getStartY()-graph.getStep());
   if(quadratic!=null && linear!=null){
    paint(false);
    }
    else{
        graph.clear()
        graph.drawCoordinateSystem();
    }
    }

function moveDown(){
    graph.setYStart(graph.getStartY()+graph.getStep());
    if(quadratic!=null && linear!=null){
        paint(false);
    }
    else{
        graph.clear()
        graph.drawCoordinateSystem();
    }
}

function zoomIn(){
    if(graph.getStep()>1){
        let newStep = graph.getStep()-1;
        graph.setStep(newStep)
        let numberOfSteps = (graph.getNumberOfSteps()-2)/2;
        graph.setXStart(graph.getStartX()+numberOfSteps);
        graph.setYStart(graph.getStartY()+numberOfSteps);
        if(quadratic!=null && linear!=null){
            paint(false);
        }
        else{
            graph.clear()
            graph.drawCoordinateSystem();
        }
    }
}

function zoomOut(){
    let newStep = graph.getStep()+1;
    graph.setStep(newStep)
    let numberOfSteps = (graph.getNumberOfSteps()-2)/2;
    graph.setXStart(graph.getStartX()-numberOfSteps);
    graph.setYStart(graph.getStartY()-numberOfSteps);
    if(quadratic!=null && linear!=null){
        paint(false);
    }
    else{
        graph.clear()
        graph.drawCoordinateSystem();
    }
    }


function writeResult(){
    if(!isNaN(result[0][0]) && !isNaN(result[0][1]) && !isNaN(result[1][0]) && !isNaN(result[1][1])){
        if(result[0][0]!=result[1][0] || result[0][1] != result[1][1]){
            resultContainer.innerHTML = "Sjeku se u točkama:<br><br>T1("+parseFloat(result[0][0]).toFixed(2)+","
                                        +parseFloat(result[0][1]).toFixed(2)+") i T2("+parseFloat(result[1][0]).toFixed(2)
                                        +","+parseFloat(result[1][1]).toFixed(2)+").";  
        }
        else{
            resultContainer.innerHTML = "Sjeku se u točki:<br><br>T("+result[0][0]+","
                                        +result[0][1]+").";
        }
    }
    else
    {
        resultContainer.innerHTML = "Ne sjeku se ni u jednoj točki.";
    }
}

function paint(autoPosition){
    graph.clear();
    if(!isNaN(result[0][0]) && !isNaN(result[0][1]) && !isNaN(result[1][0]) && !isNaN(result[1][1]) && autoPosition){
        let step = graph.getStep();
        let xDiff = Math.abs(result[0][0] - result[1][0]);
        let yDiff = Math.abs(result[0][1] - result[1][1]);
        if(xDiff>((graph.getNumberOfSteps()-2)/2)*graph.getStep()*0.9){
            do{
                graph.setStep(graph.getStep()+1);
            }while(xDiff>((graph.getNumberOfSteps()-2)/2)*graph.getStep()*0.9)
        }
        if(yDiff>((graph.getNumberOfSteps()-2)/2)*graph.getStep()*0.9){
            do{
                graph.setStep(graph.getStep()+1);
            }
            while(yDiff>((graph.getNumberOfSteps()-2)/2)*graph.getStep()*0.9)
        }
        let xCor =  Math.floor((result[0][0] + result[1][0])/2);
        let yCor =  Math.floor((result[0][1] + result[1][1])/2);
        graph.setXStart(xCor-((graph.getNumberOfSteps()-2)/2)*graph.getStep());
        graph.setYStart(yCor-((graph.getNumberOfSteps()-2)/2)*graph.getStep());
    }
    graph.drawCoordinateSystem();
    drawLinear();
    drawQuadratic();
}

function drawLinear(){

    let startX = graph.getStartX();
    let startY = graph.getStartY();
    let endX = graph.getStartX() + graph.getStep()*(graph.getNumberOfSteps()-2);
    let endY = graph.getStartY() + graph.getStep()*(graph.getNumberOfSteps()-2);

    if(!isNaN(linear.getY(graph.getStartX()))){
        graph.drawLine(startX,linear.getY(startX),
        endX,linear.getY(endX));
    }
    else{
        graph.drawLine(linear.getX(startX), startY,
        linear.getX(endY),endY);
    }
}

function drawQuadratic()
{
    let startX = graph.getStartX();
    let startY = graph.getStartY();
    let endX = graph.getStartX() + graph.getStep()*(graph.getNumberOfSteps()-2);
    let endY = graph.getStartY() + graph.getStep()*(graph.getNumberOfSteps()-2);


    let arrayX1 = [];
    let arrayX2 = [];

    let arrayY1 = [];
    let arrayY2 = [];

    let precision = 2000;

    let smallStep = (endX-startX)/precision;

    for(let i = startX; i<endX; i += smallStep){
        let y1 = quadratic.getY(i)[0];
        let y2 = quadratic.getY(i)[1];
        if(y1<endY && y1>startY){
            arrayX1.push([i,y1]);
        }
        if(y2<endY & y2>startY){
            arrayX2.push([i,y2]);
        }
    }
    for(let j = startY; j<endY; j += smallStep){
        let x1 = quadratic.getX(j)[0];
        let x2 = quadratic.getX(j)[1];
        if(x1<endX && x1>startX){
            arrayY1.push([x1,j]);
        }
        if(x2<endX && x2>startX){
            arrayY2.push([x2,j]);
        }
    }

        graph.drawCurve(arrayX1);
        graph.drawCurve(arrayX2);
        graph.drawCurve(arrayY1);
        graph.drawCurve(arrayY2);
    

}

function isQuadraticSolvable(){
    let values = quadratic.getEquationValues();
    let parameter = values[0][0]*values[1][0]*values[4][0];
    if(parameter>=0 && values[0][0]*values[1][0]>0){
        return false;
    }
    return true;
}

function findUniqueLetters(inputString) {
    const uniqueLetters = {};
    for (let i = 0; i < inputString.length; i++) {
        const character = inputString[i].toLowerCase(); 
        if (/[a-z]/.test(character) && !uniqueLetters[character]) {
            uniqueLetters[character] = true;
        }
    }
    const uniqueLetterArray = Object.keys(uniqueLetters);
    return uniqueLetterArray;
}


function sortAndMerge(arr1, arr2) {
    const mergedSet = new Set([...arr1, ...arr2]);
    const mergedAndSorted = Array.from(mergedSet).sort();
    return mergedAndSorted;
}